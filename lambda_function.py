import boto3
import datetime
import getopt
###############################################################
# leifCleanAwsEc2Snapshots                                    #
# Script will delete all snapshots created before dateLimit.  #
#                                                             #
# ALL SNAPSHOTS OLDER THAN THIS DATE WILL BE DELETED!!!       #
dateLimit = datetime.datetime(2020, 1, 1)      # yyyy, mm, dd #
###############################################################

#AWS Settings
client = boto3.client('ec2',region_name='us-west-1')
snapshots = client.describe_snapshots(OwnerIds=['1111111111111'])

def lambda_handler(event, context):
    
    # Calculate the number of days ago the date limit is.
    dateToday = datetime.datetime.now()
    dateDiff=dateToday-dateLimit
    
    # Could base this clean-up on the number of snapshots too.
    snapshotCount=len(snapshots['Snapshots'])
    print("Snapshots" + str(snapshotCount))
    
    deleteCounter = 0
    for snapshot in snapshots['Snapshots']:
        a=snapshot['StartTime']
        b=a.date()
        c=datetime.datetime.now().date()
        d=c-b
        try:
            if d.days>dateDiff.days:
                id=snapshot['SnapshotId']
                started=snapshot['StartTime']
                print(id + "********************")
                print(started)
                # Comment or Uncomment the below line to toggle "dry run".
                #client.delete_snapshot(SnapshotId=id)
                deleteCounter+=1
                print("DELETED^^^^^^^^^^^^^^^^^^")
        except getopt.GetoptError as e:
            if 'InvalidSnapshot.InUse' in e.message:
                print("skipping this snapshot")
                continue
            
    #deleteCounterStr=str(deleteCounter)
    print("\nNumber Deleted: "+str(deleteCounter)+"\n\n")
