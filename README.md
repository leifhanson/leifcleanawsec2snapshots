**lambda_function.py**

1. Set the dateLimit variable on line 9.
2. Toggle "dry run" or "live run" by commenting or uncommenting line 39.

---

## AWS Setup

1. Create an AWS Lambda script.
2. Copy and paste the lambda_function.py python code into the Lambda function.
3. Give the Lambda funciton a name, set defaults.
4. Execution time can work in as little as 10 seconds or as much as the 15 minute AWS hard cap depending how many snapshots match the dateLimit criteria for deletion.
5. Set the "test" return criteria to "Hello World" default, they don't matter.
6. Create an IAM Role for Lambda with the AWSXRayDaemonWriteAccess, AWSLambdaBasicExecutionRole, and AmazonEC2FullAccess policies.
7. Back in Lambda attach that IAM Role to your Lambda function.
8. Set the region_name in line 13 to your region name.
9. Set the OwnerIds in line 14 to your OwnerId(s).

You should now be ready to perform a dry run.

1. In Lambda, Click **SAVE** on the top.
2. Then **TEST** to run the script for your execution duration.

View the log results. If they match what you expect for the dateLimit you set, uncomment line 39, Save, and Test to perform a "Live Run".

## Disclaimer
With great power comes great responsibility. Misconfiguring this script, and/or running it without doing a dry run first, especially with an execution time over a few seconds, can absolutely delete every single snapshot you have without any further warnings or pop-up dialogues.

DO NOT UNCOMMENT LINE 39 "client.delete_snapshot(SnapshotId=id)" UNTIL YOU HAVE PLAYED WITH THE DRY RUN FUNCTIONALITY AND VIEWED SEVERAL LOGS AND UNDERSTAND WHAT WILL HAPPEN IN A LIVE RUN